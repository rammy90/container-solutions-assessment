#pull base image python
FROM python:3.6

#copy application files
COPY . /app

#set working directory
WORKDIR /app

#install python packages
RUN pip install -r requirements.txt

ENTRYPOINT ["python"]
CMD ["app.py"]