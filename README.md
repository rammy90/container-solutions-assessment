This document has been created to support container solutions assessment.

As part of the assessment, I have create the following:

1.  Python API:

    A Flask API has been created to work with the postgresSQL and API is capable to perform the following actions.
    
    *  CREATE TABLE:
        
        API will create the table in postgresSQL database
        
    *  LOAD TABLE:
    
        API will load the data "titanic.csv" to the postgres database
        
    *  UPDATE TABLE:
    
        API will update the data based on the where conditon on the postgres database
        
    *  READ TABLE:
    
        API will read the records from the database (ie top 10 rows) from the database
        
    *  DELETE TABLE:
    
        API will delete the records based on the query condition defined
        
    Note: API is created to run on localhost, so we need to open neccessary ports when running on docker containers
    
2. Docker Image for Python API:

    A docker image has been created to run the Python API.
    
    dockerfile: Used to build docker image
    requirements.txt: Used to specify and install python packages
    
    The docker image has been pushed to Docker Hub. Use the following command to pull the image.
    
    "docker push rammy90/containersolutions:assessment"
    
3. Docker Image for Postgres db:

    A docker image has been created for postgres database, however it is not working as expected. So I would not be able to complete the assessment.
    
    Note: I tried my level best to resolve the issue and complete the assessment.
    
    